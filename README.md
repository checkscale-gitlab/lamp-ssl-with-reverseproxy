# LAMP-SSL-WithReverseProxy

basic docker-compose.yml configuration for LAMP stack
this template include Let's Encrypt(SSL) and using nginx reverse proxy

คอนฟิค docker compose สร้าง `LAMP` stack ใช้ `Apache` เป็น webserver 
มีบริการ SSL (จาก Let's Encrypt) และใช้ nginx เป็น reverse proxy

# config & startup
- เปลี่ยนชื่อโฟลเดอร์ `site` เป็นชื่อที่ต้องการ
- เก็บไฟล์เวปไว้ที่ site/public_html
- สร้าง network `my_webproxy`
```
docker create network my_webproxy
```
- รัน docker-compose ที่ nginx_proxy_letsencrypt 
```
docker-compose up -d
```

- เวปแอปให้ตั้งค่า และเรียกใช้ docker-compose ตามปกติ
```
VIRTUAL_HOST: ${VIRTUAL_HOST_NAME}
LETSENCRYPT_HOST: ${SSL_HOST_NAME}
```


`TKVR`
